#!/usr/bin/env bash

set -e
# Download whole video
if [ ! -f "CNR-EXT_FULL_IMAGE_1000x750.tar" ]; then
	wget http://cnrpark.it/dataset/CNR-EXT_FULL_IMAGE_1000x750.tar
fi

if [ ! -f "CNR-EXT-Patches-150x150.zip" ]; then
	wget http://cnrpark.it/dataset/CNR-EXT-Patches-150x150.zip
fi

if [ ! -d "cnrlabels" ]; then
	unzip "CNR-EXT-Patches-150x150.zip" "LABELS/*" -d "cnrlabels"
fi

if [ ! -d "b4raw" ]; then
	mkdir b4raw
	tar -C b4raw -xvf CNR-EXT_FULL_IMAGE_1000x750.tar
fi

mkdir -p b4files
for f in b4raw/FULL_IMAGE_1000x750/SUNNY/*
do
	cp $f/$1/* b4files
done
cp b4raw/$1.csv b4files
