#!/usr/bin/env python3
# Gives all the images the same size
import argparse
parser = argparse.ArgumentParser(description="scale all images to the same size")
parser.add_argument("folder", help="folder to recursively resize images in")
parser.add_argument("-i", "--ignoreaspect", help="Scales the image to fit, but deforms the image in the process", action="store_true")

args = parser.parse_args()
import cv2
import os
import sys
import fnmatch

images = []

maxwidth = 0
maxaspect = 0

for folder, _, files in os.walk(args.folder):
    # For every png file
    imgnames = fnmatch.filter(files, "*.png")
    print(f"{folder}: {len(imgnames)} images")

    for fn in imgnames:
        # Read shape
        fn = os.path.join(folder, fn)
        img = cv2.imread(fn)
        (h, w, _) = img.shape

        # Update max
        if w > maxwidth:
            maxwidth = w

        aspect = h / w
        if aspect > maxaspect:
            maxaspect = aspect

        # Save the file name and size for later
        images.append(fn)

print(f"Maxwidth: {maxwidth}, maxaspect: {maxaspect}")

print(f"Resizing {len(images)} images")

(dw, dh) = (maxwidth, int(maxwidth * maxaspect))
print(f"Final image size: ({dw}, {dh})")

scaled = 0
skipped = 0

for i, fn in enumerate(images):
    print(f" {i}/{len(images)}\r", end='')
    img = cv2.imread(fn)
    (h, w, _) = img.shape
    if w == dw and h == dh:
        skipped += 1
        continue

    scale = dw / w

    if args.ignoreaspect:
        (nw, nh) = (dw, dh)
    else:
        (nw, nh) = (dw, int(h * scale))
    #print(f"{fn} scale with {scale} giving size ({nw}, {nh})")
    if nh > dh:
        print(f"{fn} ERROR wont fit, skipping")
        continue

    # Scale without changing aspect
    img = cv2.resize(img, (nw, nh))
    # Add black border to change the aspect
    if not args.ignoreaspect:
        img = cv2.copyMakeBorder(img, 0, dh - nh, 0, 0, cv2.BORDER_CONSTANT, 0)

    # Write it back
    cv2.imwrite(fn, img)
    scaled += 1

print()

print(f"Skipped {skipped} images and scaled {scaled} images")
