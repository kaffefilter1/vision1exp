#!/usr/bin/env python3
# Lets you move stuff between train valid and test
import os
import sys
import random
from pathlib import Path

import argparse
parser = argparse.ArgumentParser(description="moves images arround in datasets")
parser.add_argument("dataset", help="dataset folder")

args = parser.parse_args()

def statset(data):
    if data is None:
        print("Empty")
        return

    total = 0
    for cls, files in data.items():
        print(f"Class {cls}: {len(files)} images")
        total += len(files)

    print(f"Total: {total}")

def walkset(path):
    """
    Collects the dataset inside basedir
    Classes are subfolders of basedir
    Filenames are returned in a dictionary
    """
    if not os.path.isdir(path):
        return
    data = {}
    print(f"Searching {path}")
    
    for folder in os.listdir(path):
        print(f"Cound category {folder}")

        # Collect all filenames in dir
        data[folder] = [os.path.join(path, folder, f)
                for f in os.listdir(os.path.join(path, folder))]

    return data

def mergeset(data):
    res = {}

    # For set
    for s, sv in data.items():
        if sv is None:
            continue
        # For class
        for c, cv in sv.items():
            if c in res:
                res[c].extend(data[s][c])
            else:
                res[c] = cv
    return res


basedir = args.dataset

data = {}

# Lets start with some statistics
data["train"] = walkset(os.path.join(basedir, "train"))
data["valid"] = walkset(os.path.join(basedir, "valid"))
print()

print("Train set")
statset(data["train"])
print("Valid set")
statset(data["valid"])

data = mergeset(data)

print("Merged set")
statset(data)

numtrain = int(input("How many to train: "))
numvalid = int(input("How many to valid: "))

filelist = []
for c, cv in data.items():
    filelist.extend(cv)

if numtrain + numvalid > len(filelist):
    print("You cant select more than there are available")
    os.exit(1)

def move(num, to):
    for i in range(num):
        index = random.randrange(0, len(filelist))
        filename = filelist[index]
        rel = os.path.relpath(filename, os.path.join(basedir))
        # Remove the set name part
        rel = '/'.join(rel.split("/")[1:])

        # Copy it
        newname = os.path.join(basedir, to, rel)

        Path(os.path.dirname(newname)).mkdir(parents=True, exist_ok=True)

        print(f"{filename} \t -> {newname}")
        os.rename(filename, newname)

        # Remove it form the list
        del filelist[index]

move(numtrain, "train")
move(numvalid, "valid")
