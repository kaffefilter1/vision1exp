#!/usr/bin/env python3

import os
import argparse
import re
import sys
import os.path as path
from pathlib import Path
from shutil import copyfile

defaultregex = "(201\d-\d\d-\d\d)_(\d\d)(\d\d)\....(\d*)\...."
cnrregex = ".*/201\d-\d\d-\d\d/camera\d/\w_(201\d-\d\d-\d\d)_(\d\d).(\d\d)_C\d\d_(\d*)\...."

class Camloader:
    def __init__(self, fregex=defaultregex):
        self.fprog = re.compile(fregex)
        self.classdef = {}

        self.cnrprog = re.compile(cnrregex)

    def loadcamfile(self, camfile, weather):

        # Load cam file
        with open(camfile, "r") as f:
            for line in f:
                if line.endswith("\n"):
                    line = line[:-1]
                if weather != None:
                    if weather not in line:
                        continue
                line = line.split(" ")
                key = self.cnrprog.match(line[0])
                if key == None:
                    continue
                key = ''.join(key.groups())
                self.classdef[key] = line[1]


    def clsdeffromfname(self, filename):
        res = self.fprog.match(filename)
        if res == None:
            return None

        grps = res.groups()

        # Not very fast, maybe use a map instead when loading
        key = ''.join(grps)
        if key not in self.classdef:
            return None
        
        return key, self.classdef[key]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="load classification from cnr")
    parser.add_argument("camfile", help="cnr camera label file")
    parser.add_argument("datafolder", help="dataset folder to apply classification to")
    parser.add_argument("--dataset", "-d", help="dataset to apply to", default="train")
    parser.add_argument("--sortby", "-s", help="regex with groups which label name must contain", default=defaultregex)
    parser.add_argument("--weather", "-w", help="only use labels for weather")
    parser.add_argument("--fetch", "-f", help="fetch occupancy status for filename")

    args = parser.parse_args()

    loader = Camloader(args.sortby)
    loader.loadcamfile(args.camfile, args.weather)

    if args.fetch != None:
        print(loader.clsdeffromfname(args.fetch))
        sys.exit(0)

    # Stats
    occupied = 0
    empty = 0

    def classify(filename, cls):
        dest = path.join(args.datafolder, args.dataset, cls)
        Path(dest).mkdir(parents=True, exist_ok=True)


        copyfile(path.join(args.datafolder, filename),
                path.join(dest, filename))

    # Rename files
    files = os.listdir(args.datafolder)
    for i, filename in enumerate(files):
        print(f" {i+1}/{len(files)}\r", end='')

        # Not very fast, maybe use a map instead when loading
        clsdef = loader.clsdeffromfname(filename)
        if clsdef == None:
            continue
        if int(clsdef[1]) == 1:
            occupied += 1
            classify(filename, "occupied")
        else:
            empty += 1
            classify(filename, "empty")
        
    print(f"Copy stats: occupied={occupied}, empty={empty}")
