let
  pkgs = import <nixpkgs> {};
  stdenv = pkgs.stdenv;

in stdenv.mkDerivation {
  name = "visionp4-env";

  buildInputs = with pkgs; [
    python37
    python37Packages.numpy
    python37Packages.matplotlib
  ];
}
