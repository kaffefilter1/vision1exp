#!/usr/bin/env python3
import json
import os
import sys

scaler = 1000 / 2592

# scalepoint
def sp(coord):
    return int(int(coord) * scaler)

def ftopoints(fields):
    iden, x, y, w, h = fields
    point = { "id": iden, "corners": []}

    point["corners"].append( { "x": sp(x), "y": sp(y)})
    point["corners"].append( { "x": sp(x), "y": sp(y) + sp(h)})
    point["corners"].append( { "x": sp(x) + sp(w), "y": sp(y) + sp(h)})
    point["corners"].append( { "x": sp(x) + sp(w), "y": sp(y)})

    return point

def loadfile(name):
    if name.endswith(".json"):
        points = {}
        with open(name, "r") as f:
            points = json.load(f)

        return points
    elif name.endswith(".csv"):
        points = []
        with open(name, "r") as f:
            for i, line in enumerate(f):
                # Skip header
                if i == 0:
                    continue
                fields = line.split(",")

                points.append(ftopoints(fields))                
            
        return points
    else:
        print("Sorry could not determine input format")
        sys.exit(1)

def savefile(name, data):
    if name.endswith(".json"):
        with open(name, "w") as f:
            json.dump(data, f, indent="\t")
    elif name.endswith(".csv"):
        print("Export to csv not supported")
        sys.exit(1)
    else:
        print("Sorry could not determine output format")
        sys.exit(1)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="convert pointfile definitions")
    parser.add_argument("infile", help="input file")
    parser.add_argument("outfile", help="output file")
    
    args = parser.parse_args()

    data = loadfile(args.infile)
    savefile(args.outfile, data)


