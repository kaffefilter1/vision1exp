Lidt experimentering med computer vision.

# Dependencies

- python3
- opencv samt opencv-python bindings
- python-gobject og gtk3
- numpy
- fastai

## If using [Nix](https://nixos.org/nix/)

BEWARE: Order matters due to https://github.com/NixOS/nixpkgs/issues/270

0. Install python3.7

1. `virtualenv <name>`

2. `source <name>/bin/activate`

3. `pip3.7 install fastai`

4. `nix-shell`


There is also `nix-shell timelyzer.nix`.
This is since the gobject lib breaks gtk for me (Victor) :(

# Lav dataset

Start med at hent de billeder som du vil bruge.
Dette kan være en video man splitter op i frames etc.
Hvis man gerne vil have lidt data at test på kan bruge `downloadb3.sh`.
Du kan også bruge `download4b.sh camera8`.

Her vil mappen med billeder være i `b3files`.

Start med at lav en pointfil. 
**DETTE BEHØVER MAN IKKE HVIS DER ALLEREDE FINDES EN DEF.JSON FIL.
For eksempel hvis man bruge b3 billederne og `downloadb3.sh`.**
`./pointplacer.py` kan også bruges til at redigere `def.json` filer så man smadrer ikke noget.

Her vælges et billede `b3files/0000010.png`.

```bash
./pointplacer.py b3files/0000010.png b3/def.json
```

Dette vil åbne et program hvor man kan markere pladser.

1. Vent til billedet er blevet åbnet.
2. Zoom ind så der ikke er hvidt over eller til venstre.
2. Gå til den index man ønsker at ændre ( se keybindings ).
3. Tryk på pladsens 4 hjørner i følgende order (topvenstre, bundvenstre, bundhøjre, tophøjre).
	Den eneste måde man kan se at man har placeret noget er ved at `placed` tæller op.
4. Gå til næste index og gentag.
5. Når man er færdig trykket man `s` for at gemme.
	Man kan også gemme og komme tilbage senere.

Her er nogle af de forskellige bindings.
For at flytte sig rundt i billedet bruger man vertical/horizontal scroll.
`ctrl + piletaster virker også`.

Der er en fejl i musinput, så den tæller padding med.
**HUSK derfor at zoom ind**.

| Key | Beskrivelse       |
|-----|-------------------|
| `n` | Next index        |
| `p` | Prev index        |
| `c` | Clear markers     |
| `d` | Delete index      |
| `i` | Zoom in           |
| `o` | Zoom out          |
| `s` | Save to json file |

Når man har lavet sin `def.json` fil er det tid til at crop.

BEWARE: Create the output directory before running the script.

```bash
./croptool.py -c b3/def.json b3files/0000010.png /tmp/test
```

Check at billederne i `/tmp/test` passer med ens markeringer.
Nu gør man det for alle de billeder man har.

```bash
# Hvert billede
ls b3files | xargs -I % ./croptool.py -c b3/def.json -p % b3files/% b3

# Hver 10. billede
ls b3files | awk 'NR % 10 == 0' | xargs -I % ./croptool.py -c b3/def.json -p % b3files/% b3
```

Dette vil gemme alle billeder i `b3`.

Nu er det tid til at classificere billederne, dette gør man med `classify.py`.

```bash
./classify.py b3
```

Dette program vil åbne et billede også trykker man om den er taget eller ikke taget.
Så vil de placere billederne i `b3/train/empty` eller `b3/train/occupied`.

Keybindings er.

| Key | Beskrivelse                             |
|-----|-----------------------------------------|
| `o` | Marker som occupied                     |
| `e` | Marker som empty					    |
| `s` | Skip billede                            |
| `b` | Gå til sidste billede                   |
| `f` | Skip til første uklassificerede billede |
| `p` | Gå et procent frem                      |

# Klargør dataset til fastai

Start med at give alle billederne den samme størrelse med `samesize.py`.
Her er det en god ide lige at tage en backup.

```bash
cp -r b3 b3back
./samesize.py b3
```

Dette vil scale alle billeder til en max width.
Og tilføjer black bars i bunden.
Hvis den istedet skal scale uden black bars kan man bruge `-i`.

```bash
# Scale without black bars, thus stretching the image
./samesize.py b3
```

Nu skal man fordele billederne på `valid` og `train` data-setne.
Her bruger man `equalize.py`.

```bash
./equalize.py b3
```

Denne vil printe lidt into og derefter skal man indtaste hvordan man vil fordele sine billeder.
Valid bruges til at check accuracy, så der behøver man ikke så mange.

# Træning af learner med dataset

Her bruges `fastai` da det er rimelig let bare at gøre noget.
Her bruges man den der hedder `fit(1)`, ved ikke helt hvad den gør og man skal sikkert bruge nogle andre parametre.

```python
# Run this in python or ipython
from fastai.vision import *

# Load data
data = ImageDataBunch.from_folder("./b3")
learn = cnn_learner(data, models.resnet18, metrics=accuracy)

# Train
learn.fit(1)
```

Lad det køre i lidt tid.

Nu kan man teste det med nogle billeder.

```python
img = open_image("b3/0000710.png0.png")

learn.predict(img)
```

Hvis man er tilfreds med sit trænede netwærk skal man gemme det.

```python
# Default location is ./b3/export.pkl
learn.export()
```

# Kør af dataset

Før at lav et overlay på et billede kan man bruge `predictmult.py`.

```bash
# For et enkelt billede
./predictmult.py -o b3out -c b3/def.json -l b3 b3files/0000001.png

# For to billeder
./predictmult.py -o b3out -c b3/def.json -l b3 b3files/0000001.png b3files/0000002.png

# For en hel mappe
./predictmult.py -o b3out -c b3/def.json -l b3 b3files/*

# For hver 10. i en mappe
find b3files -type f | awk 'NR % 10 == 0' | ./predictmult.py -o b3out -c b3/def.json -l b3
```

Ting bliver gemt i `b3out`.
