#!/usr/bin/env python3

import argparse
import argcomplete
    
parser = argparse.ArgumentParser(description="run trained image recognition on images")
parser.add_argument("--overlay", "-o", help="save image overlays")
parser.add_argument("--config", "-c", default="def.json", help="point config file")
parser.add_argument("--learn", "-l", default=".", help="where the learn path is")
parser.add_argument("--accuracy", "-a", help="measure accuracy with specified camfile")
parser.add_argument("images", nargs="*", help="input images")

argcomplete.autocomplete(parser)

args = parser.parse_args()
# Koden her vil bruge punkterne som topleft -> buttonleft -> buttonright -> topright

import json
import cv2
import sys
import os
import fastai.vision as fv
import os.path as path
from pathlib import Path
import numpy as np
import cnrloadlabels as labels

# Loaded from current dir
import perspek
import squareplacer

if args.overlay == None:
    print("We currently only support overlay output sorry.")
    sys.exit(1)

pointfile = args.config
learnpath = args.learn
outfolder = args.overlay

Path(outfolder).mkdir(parents=True, exist_ok=True)

points = {}

# Load config file
with open(pointfile, "r") as f:
    points = json.load(f)

# Load trained neural network thingy
learn = fv.load_learner(learnpath)

images = []
if len(args.images) > 0:
    images = args.images
else:
    print("Loading files from stdin")
    for line in sys.stdin:
        if line.endswith('\n'):
            line = line[:-1]
        images.append(line)
images.sort()
#images = images[::2]

total = 0
correct = 0
loader = labels.Camloader()
if args.accuracy != None:
    loader.loadcamfile(args.accuracy, "SUNNY")

stats = [0, 0]

for i, imgname in enumerate(images):
    imgpath = imgname
    outpath = os.path.join(outfolder, os.path.basename(imgname))

    print(f" {i}/{len(images)}", end='')
    if args.accuracy != None:
        print(f" total={total}, faulty={total - correct}\r", end='')
    else:
        print("\r", end='')


    if os.path.exists(outpath):
        continue

    # Load input image
    img = cv2.imread(imgpath)
    overlay = img.copy()

    # For each point generate an output image
    for point in points:
        #print(point)
        warped = perspek.fourp_transform(img, point["corners"])

        #cv2.imwrite("/tmp/hej.png", warped)
        # Convert image to fastai image
        warped = cv2.cvtColor(warped, cv2.COLOR_BGR2RGB)
        warped = fv.Image(fv.pil2tensor(warped, dtype=np.float32).div_(255))

        # Predici
        cls, _, certain = learn.predict(warped)

        if args.accuracy != None:
            fname = path.basename(imgname) + f"{point['id']}.hah"
            correctcls = loader.clsdeffromfname(fname)

            if correctcls == None:
                print(f"Could not find label fname: {fname}, imgname: {imgname}")
            else:
                if not ((int(correctcls[1]) == 1) ^ (str(cls) == "occupied")):
                    correct += 1
                total += 1

        contours = np.empty((len(point["corners"]), 2), dtype=np.int)
        for i, pt in enumerate(point["corners"]):
            contours[i] = [ pt["x"], pt["y"]]

        print(str(cls))
        if str(cls) == "occupied":
            certain = certain[1]
            stats[0] += 1
            squareplacer.placepolygon(overlay, point, color=(0, 0, 255), thickness=-1)
        else:
            certain = certain[0]
            stats[1] += 1
            squareplacer.placepolygon(overlay, point, color=(0, 255, 0), thickness=-1)
        certain = float(certain)
        certain = int(certain*100)

        pos = (point["corners"][1]["x"] + 2, point["corners"][1]["y"] - 2)
        cv2.putText(overlay, str(certain), pos, cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255))

    squareplacer.applyoverlay(img, overlay, 0.5)
    # Write the image in a new location
    cv2.imwrite(outpath, img)

print("[occupied, empty]:", stats)
print()
if args.accuracy != None:
    print(f"Correct={correct}, total={total}")
    if total > 0:
        print(f"Accuracy: {correct / total}")
