#!/usr/bin/env python3
import argparse
import json

parser = argparse.ArgumentParser(description="Does stuff with simulator")
parser.add_argument("--simfile", "-f", default="simdata.json")
parser.add_argument("--mean", "-m", action="store_true")
parser.add_argument("--average", "-a", action="store_true")
parser.add_argument("--no-repl", action="store_true")

args = parser.parse_args()

# Load data
dump = {}
with open(args.simfile) as f:
    dump = json.load(f)

resmap = {}
for key, val in dump["resmap"].items():
    resmap[float(key)] = val
scatters = dump["scatt"]

# Create some functions
def average():
    total = 0
    totalcount = 0
    for val, count in resmap.items():
        total += val*count
        totalcount += count

    return total / totalcount

def mean():
    vals = []
    for val, count in resmap.items():
        vals += [val] * count

    vals.sort()
    return vals[int(len(vals)/2)]

if args.mean:
    print("Mean:", mean())
if args.average:
    print("Average:", average())

if not args.no_repl:
    import IPython
    IPython.embed()


