#!/usr/bin/env python3
import argparse
import json
from datetime import datetime
from datetime import timedelta

parser = argparse.ArgumentParser()
parser.add_argument("input")
parser.add_argument("output")

args = parser.parse_args()

fixed = timedelta(minutes=30)

# Load input data
data = {}
with open(args.input) as f:
    data = json.load(f)

for spot, dat in data.items():
    shift = timedelta(0)
    newdat = {}
    prev = None
    for i, time in enumerate(dat.keys()):
        timestr = time
        time = datetime.strptime(time, '%Y-%m-%d_%H%M') 
        if prev == None:
            prev = time
            continue
        shift += (time - prev) - fixed
        prev = time
        newdat[(time - shift).strftime('%Y-%m-%d_%H%M')] = dat[timestr]
    data[spot] = newdat

with open(args.output, "w") as f:
    json.dump(data, f)
