#!/usr/bin/bash

set -e
# Download whole video
if [ ! -f "b3.mp4" ]; then
	youtube-dl -f 22 https://www.youtube.com/watch?v=SUseaJu9DIE --output b3.mp4
fi

mkdir b3files
ffmpeg -i b3.mp4 -f image2 "b3files/%07d.png"
