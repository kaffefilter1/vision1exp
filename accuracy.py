#!/usr/bin/env python3

import argparse
parser = argparse.ArgumentParser(description="measure accuracy of trained ai")
parser.add_argument("learn", help="learn export folder")
parser.add_argument("datafolder", help="dataset folder")
parser.add_argument("--dataset", "-d", help="dataset name", default="valid")

args = parser.parse_args()

import fastai.vision as fv
import os
import os.path as path

# Load learner
learn = fv.load_learner(args.learn)

def test(cls):
    folder = path.join(args.datafolder, args.dataset, cls)
    total = 0
    correct = 0

    files = os.listdir(folder)
    for i, filename in enumerate(files):
        accuracy = 0
        if total > 0:
            # Maybe a bit slower but looks cool
            accuracy = correct / total
        print(f" {i}/{len(files)} faulty={total - correct}, accuracy={accuracy}\r", end='')
        img = fv.open_image(path.join(folder, filename))
        clspredict, _, _ = learn.predict(img)

        if str(clspredict) == cls:
            correct += 1
        total += 1
    accuracy = correct / total
    print(f"{cls}: total={total}, correct={correct}, accuracy={accuracy}")

    return total, correct, accuracy

# Test all classes
total = 0
correct = 0
classes = []

folder = path.join(args.datafolder, args.dataset)
for filename in os.listdir(folder):
    classfolder = path.join(args.datafolder, args.dataset, filename)
    if path.isdir(classfolder):
        t, c, _ = test(filename)
        total += t
        correct += c

        classes.append(filename)

print(f"Statistics over classes: {classes}")
print(f"total={total}, correct={correct}")

if total > 0:
    print(f"accuracy is then {correct / total}")
