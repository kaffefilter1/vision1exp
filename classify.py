#!/usr/bin/env python3

# Makes it semi easy to classify image data
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
from gi.repository.GdkPixbuf import Pixbuf, InterpType
import os
import os.path
import sys
from pathlib import Path
from shutil import copyfile

import argparse

parser = argparse.ArgumentParser(description="helps classifying datasets")
parser.add_argument("folder", help="which folder to operate on")
parser.add_argument("-d", dest="data", help="name of dataset", default="train")

args = parser.parse_args()

class Gui(Gtk.Window):
    def __init__(self, foldername, outrel):
        Gtk.Window.__init__(self, title="Hello world")

        self.foldername = foldername
        self.outrel = outrel

        self.imgindex = 0
        self.images = [f for f in os.listdir(foldername) if f.endswith(".png")]
        self.images.sort()

        self.connect("key-press-event", self.onkeydown)

        box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        self.add(box)

        self.image = Gtk.Image()
        box.pack_end(self.image, True, True, 0)

        self.statusline = Gtk.Label()
        box.pack_start(self.statusline, False, False, 0)

        self.updateimage(0)

    def updateimage(self, index):
        print(index)
        if index >= len(self.images) or index < 0:
            return

        self.imgindex = index
        self.image.set_from_file( 
                os.path.join(self.foldername, self.images[self.imgindex]))
        self.updatelabel()

    def updatelabel(self):
        (empty, occupied) = self.gettags(["empty", "occupied"])

        self.statusline.set_text(f"Name: { self.images[self.imgindex] }, \
                empty: {empty}, occupied: {occupied}, \
                index: {self.imgindex}/{len(self.images)}")

    def gettags(self, tags):
        res = []
        for tag in tags:
            tagged = os.path.exists( os.path.join(
                self.foldername, 
                self.outrel,
                tag,
                self.images[self.imgindex]
            ))
            res.append(tagged)

        return res


    def clearclass(self, cls):
        filename = os.path.join(self.foldername, self.outrel, cls, self.images[self.imgindex])
        if os.path.exists(filename):
            os.remove(filename)

    def classify(self, cls):
        clsfolder = os.path.join(self.foldername, self.outrel, cls)
        # Create the dir
        Path(clsfolder).mkdir(parents=True, exist_ok=True)

        # Copy the image
        copyfile(os.path.join(self.foldername, self.images[self.imgindex]),
                os.path.join(clsfolder, self.images[self.imgindex]))

    def onkeydown(self, widget, event):
        print(event.keyval)
        if event.keyval == 111:
            # set occupied
            self.clearclass("empty")
            self.classify("occupied")
            self.updateimage(self.imgindex + 1)
        if event.keyval == 101:
            # set empty
            self.clearclass("occupied")
            self.classify("empty")
            self.updateimage(self.imgindex + 1)
        if event.keyval == 102:
            # Fast forward to first untagged
            for i in self.images:
                (empty, occupied) = self.gettags(["empty", "occupied"])
                if not (empty or occupied):
                    break
                self.updateimage(self.imgindex + 1)
        if event.keyval == 115:
            # skip
            self.updateimage(self.imgindex + 1)
        if event.keyval == 112:
            # one percent forward
            self.updateimage(self.imgindex + int(len(self.images) / 100))
        if event.keyval == 98:
            # back
            self.updateimage(self.imgindex - 1)

win = Gui(args.folder, args.data)
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()

