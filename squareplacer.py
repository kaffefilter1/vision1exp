#!/usr/bin/env python3

import cv2
import configloader
import numpy as np

def placepolygon(dest, point, color=(255, 255, 255), thickness=-1):
    contours = np.empty((len(point["corners"]), 2), dtype=np.int)
    for i, pt in enumerate(point["corners"]):
        contours[i] = [ pt["x"], pt["y"]]

    cv2.drawContours(dest, [contours], 0, color, thickness)

def applyoverlay(dest, overlay, alpha):
    cv2.addWeighted(overlay, alpha, dest, 1 - alpha, 0, dest)

if __name__ == "__main__":
    import argparse
    import os

    parser = argparse.ArgumentParser(description="draws spots from pointfile")
    parser.add_argument("pointfile", help="file to read points from")
    parser.add_argument("input", help="input image")
    parser.add_argument("output", help="output image")
    parser.add_argument("--color", "-c", help="overlay color", default="#000000")
    parser.add_argument("--border", "-b", help="border thickness, -1 for no border", type=int, default=2)
    parser.add_argument("--alpha", "-a", type=float, default=0.5)
    parser.add_argument("--writeid", "-i", help="write spot id's in specified color")

    args = parser.parse_args()

    # Load image and config
    img = cv2.imread(args.input)
    cfg = configloader.loadfile(args.pointfile)

    # Convert color code
    args.color = args.color.lstrip("#")
    # https://stackoverflow.com/questions/29643352/converting-hex-to-rgb-value-in-python
    color = tuple(int(args.color[i:i+2], 16) for i in (0, 2, 4))
    idcolor = ''
    if args.writeid != None:
        idcolor = args.writeid.lstrip("#")
        idcolor = tuple(int(args.writeid[i:i+2], 16) for i in (0, 2, 4)) 

    overlay = img.copy()
    for point in cfg:
        placepolygon(overlay, point, color=color, thickness=args.border)

        pos = (point["corners"][1]["x"] + 2, point["corners"][1]["y"] - 2)

        if args.writeid != None:
            cv2.putText(overlay, str(point["id"]), pos, cv2.FONT_HERSHEY_PLAIN, 1, idcolor)
    applyoverlay(img, overlay, args.alpha)

    cv2.imwrite(args.output, img)
