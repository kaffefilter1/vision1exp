#!/usr/bin/env python3
import json
import cv2
import sys
import perspek
import os

import argparse

parser = argparse.ArgumentParser(description="crops images from def files")
parser.add_argument("image", help="input image")
parser.add_argument("destination", help="where to place cutouts")
parser.add_argument("--config", "-c", default="def.json", help="point config file")
parser.add_argument("--prefix", "-p", default="", help="prefix of filenames inside destination")

args = parser.parse_args()

# Koden her vil bruge punkterne som topleft -> buttonleft -> buttonright -> topright

inimgname = args.image
outfoldername = args.destination
pointfile = args.config
prefix = args.prefix

points = {}

# Load config file
with open(pointfile, "r") as f:
    points = json.load(f)

# Load input image
img = cv2.imread(inimgname)

# For each point generate an output image
for point in points:
    #print(point)
    warped = perspek.fourp_transform(img, point["corners"])

    # Save the image to disk
    outfile = os.path.join(outfoldername, prefix + str(point["id"]) + ".png")
    print(outfile)
    cv2.imwrite(outfile, warped) 

