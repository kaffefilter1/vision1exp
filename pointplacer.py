#!/usr/bin/env python3
# Sorry this is a quickly hacked together script
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
from gi.repository.GdkPixbuf import Pixbuf, InterpType
import sys
import json
import os

import argparse

parser = argparse.ArgumentParser(description="edits the def config files")
parser.add_argument("image", help="image to put points on")
parser.add_argument("config", help="the outgoing config file")

args = parser.parse_args()

class Gui(Gtk.Window):
    def __init__(self, imagename, configname):
        Gtk.Window.__init__(self, title="Hello World")

        self.imagebuff = Pixbuf.new_from_file(imagename)
        self.imageaspect = self.imagebuff.get_height() / self.imagebuff.get_width()

        # Setup box
        self.box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        self.add(self.box)

        # self.connect("configure-event", self.onresize)
        self.connect("key-press-event", self.onkeydown)

        # Viewpoint settings
        self.zoom = 1.0
        self.offset = (0, 0)

        # Use scrolled window to allow resizing
        self.scrolled_window = Gtk.ScrolledWindow()

        self.image = Gtk.Image()
        self.applyImage()
        
        # Apply the image and window to main window
        eventbox = Gtk.EventBox()
        eventbox.add(self.image)
        self.scrolled_window.add(eventbox)
        eventbox.connect("button-press-event", self.imageclick)
        self.box.pack_end(self.scrolled_window, True, True, 0)

        # Setup labels
        self.pointinfo = Gtk.Label()
        self.pointinfo.set_justify(Gtk.Justification.LEFT)
        self.box.pack_start(self.pointinfo, False, False, 0)

        # Load json
        self.configname = configname
        self.pointdata = {}
        if os.path.exists(configname):
            with open(configname, "r") as f:
                self.pointdata = json.load(f)
        else:
            self.pointdata = []

        print(self.pointdata)

        # Setup point vars
        self.currentspot = 0
        self.placed = 0
        self.loadid(0)

        self.updatelabels()

    def updatelabels(self):
        available = False
        if "available" in self.pointdata[self.currentspot]:
            available = self.pointdata[self.currentspot]['available']

        self.pointinfo.set_text(f"Img: { self.currentspot }, Placed: {self.placed}, Available: { available }")

    def loadid(self, id):
        self.currentspot = id
        if id < len(self.pointdata):
            self.placed = len(self.pointdata[id]["corners"])
        else:
            # Create it
            self.pointdata.append({ "id": id, "corners": []})
            self.loadid(id)

    def onkeydown(self, widget, event):
        print(event.keyval)
        wadj = self.scrolled_window.get_vadjustment()
        hadj = self.scrolled_window.get_hadjustment()
        if event.keyval == 105:
            # Zoom in
            self.zoom *= 1.1
        elif event.keyval == 111:
            # Zoom out
            self.zoom *= 0.9
            pass
        elif event.keyval == 110:
            # Next 
            self.loadid(self.currentspot + 1)
        elif event.keyval == 112:
            # Prev 
            if self.currentspot > 0:
                self.loadid(self.currentspot - 1)
        elif event.keyval == 99:
            self.placed = 0
            self.pointdata[self.currentspot]["corners"] = []
        elif event.keyval == 100:
            del self.pointdata[self.currentspot]
            self.loadid(self.currentspot)
        elif event.keyval == 116:
            available = False
            if "available" in self.pointdata[self.currentspot]:
                available = self.pointdata[self.currentspot]['available']
            self.pointdata[self.currentspot]["available"] = not available
        elif event.keyval == 115:
            # Save
            with open(self.configname, "w") as f:
                json.dump(self.pointdata, f, indent="\t")

        self.applyImage()
        self.updatelabels()

    def applyImage(self):
        # Calculate new size
        nw = self.imagebuff.get_width() * self.zoom
        nh = self.imagebuff.get_height() * self.zoom

        # Make sure we do not go outside the image
        img = self.imagebuff.scale_simple(nw, nh, InterpType.NEAREST)

        self.image.set_from_pixbuf(img)

    def imageclick(self, widget, event):
        (x, y) = (int(event.x / self.zoom), int(event.y / self.zoom))
        print(x, y)
        # Add a corner
        self.pointdata[self.currentspot]["corners"].append({ "x": x, "y": y})
        self.placed += 1
        self.updatelabels()

win = Gui(args.image, args.config)
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
