#!/usr/bin/env python3
import json
import fileinput
import argparse
from matplotlib import pyplot as plt
import numpy as np
from datetime import datetime
from datetime import timedelta
import random
import multiprocessing as mp


def toTime(stampstr):
    return datetime.strptime(stampstr, '%Y-%m-%d_%H%M')

def chStampKey(atruth, f):
    for spot in atruth:
        tmp = {}
        for stamp in atruth[spot]:
            newKey = f(stamp)
            tmp[newKey] = atruth[spot][stamp]
        atruth[spot] = tmp

def initTruth(f):
    jason = open(f, 'r')
    truth = json.loads(jason.read())
    jason.close()
    chStampKey(truth, toTime)
    return truth

def gen_shift(m=2):
    """
    Return a function that shifts timestamps by m minutes.
    """
    def shift(s):
        """
        Returns a timestamp shifted my m minutes.
        """
        d = timedelta(minutes=m)
        s += d
        return s
    return shift

debug = False
def dprint(*args, **kwargs):
    if debug:
        print(*args, **kwargs)

def calcAcc(truth, atruth, pacAcc=1):
    """
    Assumes sorted timestamps.
    (done by dirjsonner.py)
    """
    errorPercentages = []
    for spot in truth:
        startTime = datetime(9999, 12, 31, 23, 59)
        stopTime = datetime(1, 1, 1)
        errorTime = timedelta(0)
        # shifted timestamps
        astamplist = list(atruth[spot].keys())
        astampvalues = [0] * len(astamplist)

        # unshifted timestamps
        stamplist = list(truth[spot].keys())

        truthlist = list(truth[spot].values())

        # since no timetamp == none, we are wrong during inital latency
        aindex = 0
        firstday = 0
        latency = astamplist[0] - stamplist[0]
        s = 0
        if latency > timedelta(0):
            s = int(latency / (stamplist[1]  - stamplist[0]) )   
        dprint("LAT", s)
        for i, available in enumerate(truthlist):
            dprint(stamplist[1]  - stamplist[0])
            # Needed to calc total timespan of dataset
            if stamplist[i] > stopTime:
                stopTime = stamplist[i]
            if stamplist[i] < startTime:
                startTime = stamplist[i]

            measured = available

            if random.uniform(0,1) > pacAcc:
                measured = not available
            astampvalues[i] = measured
            dprint(astampvalues)

            if i < s:
                # dprint("error 3", stamplist[i + 1] - stamplist[i])
                errorTime += stamplist[i + 1] - stamplist[i]
                continue

            # dprint("NEW")
            error = timedelta(0)

            #dprint(stamplist[i], stamplist[i+1], astamplist[i - s])
            # if PAC was wrong, we must have been wrong until next image
            if astampvalues[i - s] != available:
                # prevent out of index
                if len(stamplist) > i + 1:
                    dprint("error 2")
                    dprint("hejdsa", stamplist[i + 1], astamplist[i - s])
                    errorTime += stamplist[i + 1] - astamplist[i - s]
                    dprint("2:", stamplist[i + 1] - astamplist[i - s])

            # if state of spot changed we must have been wrong during latency
            dprint("hej", i-s-1)
            dprint(astampvalues[i - s - 1])
            if (i - s - 1) < 0 or available != astampvalues[i - s - 1]:
                err = astamplist[i - s] - stamplist[i]
                dprint("3: pre", err)
                if err > timedelta(0):
                    # dprint(astamplist[aindex], stamplist[i])
                    errorTime += err
                    dprint("3:", err)
            # dprint(error)

        dprint("errortime:", errorTime)
        totalTime = stopTime - startTime
        errorPercentage = errorTime / totalTime
        errorPercentages.append(errorPercentage)
    return 1 - (sum(errorPercentages) / len(errorPercentages))

cli = argparse.ArgumentParser(description="""
    Reads json file created by dirjsonner,
    dprints accuracy based on parameters such as latenvy
    """)
cli.add_argument("file", help="The json file to read")

args = cli.parse_args()

truth = initTruth(args.file)
#for i in range(0, 5):
#    res = calcAcc(truth, delayedTruth)
#    dprint(f'{i}: {res}')
#    chStampKey(delayedTruth, gen_shift(1))

def npJuggle(x, pacAcc):
    """
    Function for matplotlib that handles numpy type things.
    """
    res = np.empty(len(x))
    for i, s in enumerate(x):
        # dprint(f"LATENCY {s}")
        # unpack (similar to copy) truth into delayedTruth
        delayedTruth = {**truth}
        chStampKey(delayedTruth, gen_shift(int(s)))
        res[i] = calcAcc(truth, delayedTruth, pacAcc)
    return res

#x = np.arange(0, 150, 1) # start, stop, step
#pacAcc = 0.93
#y = npJuggle(x, pacAcc)
#fig, ax = plt.subplots()
#ax.plot(x,y)
#ax.axhline(y=0.8639, color="r")
#ax.set(xlabel='Constant latency in minutes',
#       ylabel='Match fraction',
#       title=F'Match fraction over constant latency, PAC accuracy of {pacAcc}')
#ax.grid()
 #plt.axis([0, 150, 0.8, 1])
#fig.savefig('latency.pdf')

#def plot_sim(y, x, ax):
#    

def mcarlostep(x, step, i):
    print(f"sim {i}")
    res = []
    firstRes = npJuggle(x, 0.93)
    for j, elem in enumerate(firstRes):
        if elem < 0.8639:
            res.append(j * step)
    return res, firstRes

def mcarlo(xLength, step, sims):
    res = None
    x = np.arange(0, xLength, step)
    args = [(x, step, i) for i in range(sims)]
    p = None
    if debug:
        p = mp.Pool(1)
    else:
        p = mp.Pool(mp.cpu_count())
    res = p.starmap_async(mcarlostep, args)
    p.close()
    p.join()

    returned = res.get()

    resmap = {}
    scatters = []
    for rs in returned:
        plt.scatter(x, rs[1], alpha=0.5, marker='.')
        scatters.append(rs[1].tolist())
        for r in rs[0]:
            if r not in resmap:
                resmap[r] = 1
            else:
                resmap[r] += 1

    return resmap, scatters


def normalize(arr):
    return [float(i)/max(arr) for i in arr]

print("Creating single scatter")
plt.figure()
mcarlo(30, 0.1, 1)
plt.xlabel('Constant latency in minutes')
plt.ylabel('Match fraction')
plt.axhline(y=0.8639, color="r")
plt.savefig('singleScattered.pdf')
plt.clf()

print("Creating 4 scatter")
plt.figure()
mcarlo(30, 0.1, 4)
plt.xlabel('Constant latency in minutes')
plt.ylabel('Match fraction')
plt.axhline(y=0.8639, color="r")
plt.savefig('fourScattered.pdf')
plt.clf()

print("Creating multi scatter")
plt.figure()
yeet, scatters = mcarlo(30, 0.1, 1024)
plt.xlabel('Constant latency in minutes')
plt.ylabel('Match fraction')
plt.axhline(y=0.8639, color="r")
plt.savefig('mScattered.pdf')
plt.clf()

print("Creating freq")
plt.figure()
plt.bar(yeet.keys(), yeet.values())
plt.ylabel('Frequency')
plt.xlabel('Latency constant')
plt.savefig('freq.pdf')
plt.clf()


print("Creating normalized freq")
plt.figure()
plt.bar(yeet.keys(), normalize(yeet.values()))
plt.ylabel('Frequency, normalized')
plt.xlabel('Latency constant')
plt.savefig('freqNormal.pdf')
plt.clf()

print("Dumping data")
data = {"resmap": yeet, "scatt": scatters}
with open("simdata.json", "w") as f:
    json.dump(data, f)
