import cv2
import numpy as np

# Implemented from https://www.pyimagesearch.com/2014/08/25/4-point-opencv-getperspective-transform-example/
def fourp_transform(img, pts):
    """
    4 point transform

    img is a opencv image
    and pts is a list of 4 images with a object in the following form { x: 123, y: 2312 }

    The values in point have the following order
    topleft -> buttonleft -> buttonright -> topright

    """
    # TODO maybe to something to ensure correct order
    (tl, bl, br, tr) = pts

    # Calculate the side lengths with pythagoras
    widthTop = np.sqrt((tr["x"] - tl["x"])**2 + (tr["y"] - tl["y"])**2)
    widthBottom = np.sqrt((br["x"] - bl["x"])**2 + (br["y"] - bl["y"])**2)
    maxWidth = max(int(widthTop), int(widthBottom))

    # Do the same with height
    heightLeft = np.sqrt((bl["x"] - tl["x"])**2 + (bl["y"] - tl["y"])**2)
    heightRight = np.sqrt((br["x"] - tr["x"])**2 + (br["y"] - tr["y"])**2)
    maxHeight = max(int(heightLeft), int(heightRight))

    # Opencv has transforms but we must provide it with 4x2 matrixes containing corner points
    # Src are the the points in the original image, 
    # Dst are the corrosponding points in the new image
    src = np.array([
        [tl["x"], tl["y"]],
        [bl["x"], bl["y"]],
        [br["x"], br["y"]],
        [tr["x"], tr["y"]]
    ], dtype="float32")
    dst = np.array([
        [0, 0],
        [0, maxHeight - 1],
        [maxWidth - 1, maxHeight - 1],
        [maxWidth - 1, 0]], dtype="float32")

    M = cv2.getPerspectiveTransform(src, dst)
    warped = cv2.warpPerspective(img, M, (maxWidth, maxHeight))

    return warped
