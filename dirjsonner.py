#!/usr/bin/env python3
from pathlib import Path
import json
import argparse
import os
import datetime
from collections import defaultdict

cli = argparse.ArgumentParser(description="Reads the fastai directory structure and turns it into json")
cli.add_argument("directory",  help="The directory to read")

args = cli.parse_args()

paths = Path(args.directory).rglob('*')

# Nested dict
jason = defaultdict(dict)
for path in paths:
    dirOf = os.path.dirname(path)
    if path.is_file() and "empty" in dirOf or "occupied" in dirOf:
        parts = os.path.basename(path).split('.')
        stampstr = parts[0]
        # to parse this stampstr, use:
        # stamp = datetime.datetime.strptime(stampstr, '%Y-%m-%d_%H%M')
        if "empty" in dirOf:
            available = True
        else:
            available = False
        identifier = parts[1]
        jason[identifier][stampstr] = available
print(json.dumps(jason, indent=2, sort_keys=True))
